# SPA Project: Algebraic equations over F2

This project aims to find if a Polynomial System have a common root.
To do it we employ method seen in class, that is SAT solving & SMT solving.
See `spa_report.pdf` for the associated report.

[Course Page](http://khalilghorbal.info/teaching/spa.html)

## Requirement

You will need `bash` and `perl` for the wraper scripts.
`ocaml` (>=v4.0.0) and `python3` with `pysmt` and `matplotlib` installed and its backend configured for the rest.

## SAT Solving

Use the wraper script `wrap-sat.sh` to
have the process stop if it exceed 60s of runtime.

Usage:
`./wrap-sat.sh <nb-polynomial> <poly_max_size> <nb_var> <poly_max_degree>`

It will generate a random instance with the specified parameters.

## SMT Solving

Use the wraper script `wrap-smt.sh` to
have the process stop if it exceed 60s of runtime.

Usage:
`./wrap-smt.sh <nb-polynomial> <poly_max_size> <nb_var> <poly_max_degree>`

## License
This repository is MIT licensed, see `COPYING` file.
Copyright 2019 Rémi Piau
