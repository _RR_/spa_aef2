#!/bin/bash

doalarm () { perl -e 'alarm shift; exec @ARGV' "$@"; }

doalarm 120 python3 aef2-smt.py "${@:1}"
