#!/usr/bin/python3
# This file is MIT licensed, see accompanying COPYING file for full license
# Copyright 2019 Rémi Piau

import random as rd
import sys
# Import necessary for smt solving
from pysmt.shortcuts import Symbol, And, GE, LE, Plus, Equals, Int, get_model
from pysmt.shortcuts import Ite, Times
from pysmt.typing import INT


# Polynomial
# [list_var_term1, ...]
# Boolean formula
# [clause_1,...]
def generate_polynomial(number_of_var, size, cdegree):
    poly = []
    size1 = size
    # One chance /2 of having a constant
    if rd.randint(0, 1) == 1:
        poly.append([number_of_var+1])
        size1 = size1 - 1
    for i in range(size1):
        nterm = rd.randint(1, cdegree)
        term_n = []
        for j in range(nterm):
            term_n.append(rd.randint(1, number_of_var))
        poly.append(list(term_n))
    return list(poly)


def poly_to_string(p):
    ps = ""
    for l in p:
        l1 = sorted(l)
        for e in l1:
            ps += "x" + e + " "
        ps += "+ "
    if ps != "":
        ps = ps[:-3]
    return ps


def poly_to_formula(poly, var_lst):
    poly_formula = Int(0)
    for clause in poly:
        clause_formula = Times([var_lst[x-1] for x in clause])
        poly_formula = Ite(Equals(Plus(poly_formula, clause_formula),
                                  Int(1)), Int(1), Int(0))
    return poly_formula


def have_common_root(poly_lst, nb_var, debug=False):
    lst_var = [Symbol("x"+str(i), INT) for i in range(1, nb_var+2)]
    poly_smt_lst = [poly_to_formula(poly, lst_var) for poly in poly_lst]
    common_root_valuation = And([Equals(p, Int(0)) for p in poly_smt_lst])
    # X_{nb_var+1} = 1 its the constant
    restricted_variable_support = And(And([And(GE(x, Int(0)), LE(x, Int(1)))
                                           for x in lst_var]),
                                      Equals(lst_var[-1], Int(1)))

    problem = And(common_root_valuation, restricted_variable_support)
    if debug:
        print("***********************")
        print("Formula:")
        print(problem)
        print("***********************")
        print("Solving")
    model = get_model(problem)
    # is there a solution
    if model:
        if debug:
            print("Found valuation:")
            print(model)
        return True
    else:
        if debug:
            print("Cannot Find valuation")
        return False


# If run as script handle args and run solver
if __name__ == "__main__":
    argc = len(sys.argv)
    if argc < 5:
        print("Help:")
        print(str(sys.argv[0]) +
              " <nb_polynomials> <size> <nb_var> <max_degree>")
        print("Add any flags after that to enable debug mode")
    else:
        nb_poly = int(sys.argv[1])
        poly_max_size = int(sys.argv[2])
        nb_var = int(sys.argv[3])
        max_degree = int(sys.argv[4])
        debug = (len(sys.argv) > 5)
        poly_lst = [generate_polynomial(nb_var, poly_max_size, max_degree)
                    for i in range(nb_poly)]
        if debug:
            print("***********************")
            print(poly_lst)
            print("***********************")
        print(have_common_root(poly_lst, nb_var, debug))
