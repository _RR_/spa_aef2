#!/usr/bin/python3
# This file is MIT licensed, see accompanying COPYING file for full license
# Copyright 2019 Rémi Piau

import os
import time
from matplotlib import pyplot as plt

# Make parameter variate
# remember <nb_polynomials> <size> <nb_var> <max_degree>


def call_sat(nb_poly, size, nb_var, max_deg):
    t0 = time.time()
    os.system("./wrap-sat.sh " + str(nb_poly) + " " + str(size)
                               + " " + str(nb_var) + " " + str(max_deg))
    ts = time.time()
    return ts-t0


def call_smt(nb_poly, size, nb_var, max_deg):
    t0 = time.time()
    os.system("./wrap-smt.sh " + str(nb_poly) + " " + str(size)
                               + " " + str(nb_var) + " " + str(max_deg))
    ts = time.time()
    return ts-t0


max_time_wrappers = 119
aboard_if_more = 5

size = 100
nb_var = 50
max_degree = 50
nb_echant = 100
max_nb_poly = 10000

finish = False
x_sat = []
x_smt = []
y_sat = []
y_smt = []
# sat
for nb_poly in range(1, max_nb_poly):
    times_now_sat = []
    for i in range(nb_echant):
        print("Nb_poly=" + str(nb_poly) + " / " + str(max_nb_poly))
        print("Instance " + str(i) + "/" + str(nb_echant))
        times_now_sat.append(call_sat(nb_poly, size, nb_var, max_degree))
        if len(times_now_sat) >= aboard_if_more:
            v = sum([times_now_sat[-i]
                     for i in range(aboard_if_more)])
            if (v / aboard_if_more) >= max_time_wrappers:
                finish = True
                break
    x_sat.append(nb_poly)
    y_sat.append(sum(times_now_sat)/len(times_now_sat))
    if finish:
        break
# smt
finish = False
x_sat = []
for nb_poly in range(1, 10000):
    times_now_smt = []
    for i in range(nb_echant):
        times_now_smt.append(call_smt(nb_poly, size, nb_var, max_degree))
        if len(times_now_smt) >= aboard_if_more:
            v = sum([times_now_smt[-i]
                     for i in range(aboard_if_more)])
            if (v / aboard_if_more) >= max_time_wrappers:
                finish = True
                break
    x_smt.append(nb_poly)
    y_smt.append(sum(times_now_smt)/len(times_now_smt))
    if finish:
        break

plt.plot(x_sat, y_sat, label="SAT solver")
plt.plot(x_smt, y_smt, label="SMT solver")
plt.legend()
plt.title("Average execution time in s in function" +
          "of the number of polynomials in the system")
plt.show()
