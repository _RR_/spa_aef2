#!/bin/bash

doalarm () { perl -e 'alarm shift; exec @ARGV' "$@"; }

if [ "$#" -eq 4 ]
then
    doalarm 120 ocaml aef2-sat.ml "${@:1}" | minisat
else
    doalarm 120 ocaml aef2-sat.ml "${@:1}"
fi
