(* This file is MIT licensed, see accompanying COPYING file for full license *)
(* Copyright 2019 Rémi Piau *)

(* Initialize rng *)
Random.self_init()
(* Types needed *)
type polynomial = int list list;;
type term = int list;;
type formula = Var of int
    | And of (formula * formula)
    | Xor of (formula * formula)
    | Not of formula
    | Or of (formula * formula);;
type clause = string list;;
type cnf = string list list;;

(* Generation and manipulation of polynomials *)
(* X_{nb_var+1} is Top *)
let rec generate_term nb_var nterm =
    match nterm with
    | 0 -> []
    | k -> (1 + (Random.int nb_var))::(generate_term nb_var (k - 1));;

let generate_polynomial size nb_var cdegree =
    (* add a constant term with 1/2 chance *)
    let rec generate_polynomial_aux size1 nb_var1 cdegree1 =
        match size1 with
        | 0 -> []
        | k -> (generate_term nb_var1 (1+(Random.int cdegree1)))::(generate_polynomial_aux (size1 - 1) nb_var1 cdegree1)
    in
    (* 1/2 of having a constant =1 *)
    let constant = Random.int 2 in
    if constant = 1 then
        ([nb_var + 1])::(generate_polynomial_aux (size-1) nb_var cdegree)
    else
        (generate_polynomial_aux size nb_var cdegree);;

let poly_to_string (p: polynomial) =
    let rec pts_clause_aux c1 =
        match c1 with
        | [] -> []
        | h::t -> (String.concat "" ["x_"; (string_of_int h)])::(pts_clause_aux t)
    in
    let rec pts_aux p1 =
        match p1 with
        | [] -> []
        | t1::q1 -> (String.concat "*" (pts_clause_aux t1))::(pts_aux q1)
    in
    (String.concat " + " (pts_aux p));;

let rec evaluate_term (c: term) (valuation: int array)=
    match c with
    | [] -> failwith "Empty clause, cannot evaluate"
    | [h] -> valuation.(h)
    | h::q -> valuation.(h)*(evaluate_term q valuation) mod 2;;

let rec evaluate_poly p (valuation: int array) =
    match p with
    | [] -> failwith "Empty polynomial, cannot evaluate"
    | [h] -> (evaluate_term h valuation)
    | h::q -> (((evaluate_term h valuation)+(evaluate_poly q valuation)) mod 2);;

(* Conversion from polynomial to formula *)
let rec term_to_formula (c: term) =
    match c with
    | [] -> failwith "Empty polynomial term"
    | [a] -> Var a
    | h::q -> (And ((Var h), (term_to_formula q)));;

let rec poly_to_formula (p: polynomial) =
    match p with
    | [] -> failwith "Empty polynomial"
    | [h] -> term_to_formula h
    | h::q -> (Xor ((term_to_formula h), (poly_to_formula q)));;

let rec formula_to_string (f: formula) =
    match f with
    | Var x -> (String.concat "" ["x_"; string_of_int x])
    | Not fa -> (String.concat "" ["\\neg"; formula_to_string fa])
    | And (fa,fb) -> (String.concat "" ["(";formula_to_string fa; " \\wedge "; formula_to_string fb;")"])
    | Or (fa,fb) -> (String.concat "" ["(";formula_to_string fa; " \\vee "; formula_to_string fb;")"])
    | Xor (fa,fb) -> (String.concat "" ["(";formula_to_string fa; " \\oplus "; formula_to_string fb;")"]);;

(* formula to CNF *)
let rec eliminate_xor (f: formula)=
    match f with
    | Var x -> Var x
    | And (fa, fb) -> And (eliminate_xor fa, eliminate_xor fb)
    | Xor (fa, fb) -> let ela = eliminate_xor fa and elb = eliminate_xor fb
                      in Or (And (Not ela, elb), And (ela, Not elb))
    | Not fa -> Not (eliminate_xor fa)
    | Or (fa, fb) -> Or (eliminate_xor fa, eliminate_xor fb);;

let rec neg_normal_form (f: formula)=
    match f with
    | Var x -> Var x
    | Not (And (fa, fb)) -> (Or (neg_normal_form (Not fa), neg_normal_form (Not fb)))
    | Not (Or (fa, fb)) -> (And (neg_normal_form (Not fa), neg_normal_form (Not fb)))
    | Not Not fa -> neg_normal_form fa
    | And (fa, fb) -> (And (neg_normal_form fa, neg_normal_form fb))
    | Xor (fa, fb) -> failwith "Formula must not contain Xor here!"
    | Or (fa, fb) -> (Or (neg_normal_form fa, neg_normal_form fb))
    | Not v -> Not v;;

(* let var_tmp = ref 0;; *)
let get_new_var_func nb_var=
    let var_tmp = ref nb_var in
    (fun () -> (var_tmp:=!var_tmp+1; !var_tmp));;

let rec tseitin (f: formula) get_var=
    match f with
    | Var x -> (Var (get_var ()), Var x)
    | Not fa -> let idnot = Var (get_var ()) and (fat, ida) = tseitin fa get_var in
                         (And (And 
                             (Or (idnot, ida), 
                             Or (Not idnot, Not ida)), fat), idnot)
    | And (fa, fb) -> let fat, ida = tseitin fa get_var and fbt, idb = tseitin fb get_var
                      and idand = Var (get_var ()) in
                      (And (And (And (Or (Not idand, ida), Or (Not idand, idb)),
                           Or (Or (Not ida, Not idb), idand)), And (fat, fbt)), idand)
    | Or (fa, fb) -> let fat, ida = tseitin fa get_var and fbt, idb = tseitin fb get_var
                      and idor = Var (get_var ()) in
                      (And (And (And (Or (idor, Not ida), Or (idor, Not idb)),
                           Or (Or (ida, idb), Not idor)), And (fat, fbt)), idor)
    | Xor (fa, fb) -> let fat, ida = tseitin fa get_var and fbt, idb = tseitin fb get_var
                      and idxor = Var (get_var ()) in
                  (And (And (And (And (Or (Or (Not idxor, Not ida), idb),
                                      Or (Or (Not idxor, ida), Not idb)),
                                      Or (Or (Not ida, Not idb), idxor)),
                                      Or (Or (ida,idb), idxor)),
                                  And (fat, fbt)), idxor);;

(* to crude, use tseitin transformation instead *)
let rec distribute_or_inward (f: formula)=
    match f with
    | Var x -> Var x
    | Not (Var x) -> Not (Var x)
    | Or (fa, And (fb1, fb2)) -> distribute_or_inward (And (Or (fa, fb1), Or(fa, fb2)))
    | Or (And (fa1, fa2), fb) -> distribute_or_inward (And (Or (fa1, fb), Or(fa2, fb)))
    | Not fa -> failwith "Not should be clause to vars ! Have you applied neg_normal_form ?"
    | Xor (fa,fb) -> failwith "Formula must not contain Xor here!"
    | And (fa, fb) -> And (distribute_or_inward fa, distribute_or_inward fb)
    | Or (fa, fb) -> Or (distribute_or_inward fa, distribute_or_inward fb);;

let formula_to_cnf_formula (f: formula) get_var=
    tseitin f get_var;;

let cnf_formula_to_cnf (f: formula) =
    let rec to_cnf_clause_aux c =
    match c with
    | Var x -> [string_of_int x]
    | Not (Var x) -> [(String.concat "" ["-"; string_of_int x])]
    | Or (ca, cb) -> (to_cnf_clause_aux ca)@(to_cnf_clause_aux cb)
    | And(c1, c2) -> failwith "And detected"
    | _ -> failwith "Formula (Clause) is not in CNF"
    in
    let rec to_cnf_formula_aux f1 =
        match f1 with
        | And (f1, f2) -> (to_cnf_formula_aux f1)@(to_cnf_formula_aux f2)
        | Or (fa, fb) -> [to_cnf_clause_aux (Or (fa, fb))]
        | Var x -> [to_cnf_clause_aux (Var x)]
        | Not (Var x) -> [to_cnf_clause_aux (Not (Var x))]
        | _ -> failwith "Formula is not in CNF"
    in
    to_cnf_formula_aux f;;

(* CNF to DMACS *)
let cnf_to_dimacs_helper (f :cnf)=
    let add_terminal s =
        (String.concat "" [s;" 0"])
    in
    let rec gen_clauses f1 =
        match f1 with
        | [] -> failwith "Empty cnf"
        | [h] -> ([add_terminal (String.concat " " h)], 1)
        | h::q -> let rest, nq = (gen_clauses q) in
                ((add_terminal (String.concat " " h))::rest,nq+1)
    in
    let cls, n = gen_clauses f in
     (String.concat "\n" cls, n);;

let cnf_to_dimacs nb_var (f_lst : cnf list)=
    let rec iter_list lst =
        match lst with
        | [] -> ([], 0)
        | h::q -> let clh, nh = cnf_to_dimacs_helper h
                  and clsq, nq = (iter_list q)
                  in (clh::clsq, nh+nq)
    in
    let clauses, n_cl = iter_list f_lst in
    String.concat "\n" ["c File generated automatically => see _RR_/aef2 repository";
     (String.concat " " ["p cnf"; string_of_int nb_var; string_of_int (n_cl)]);
     (String.concat "\n" clauses)];;
(** Scripting part handle args generate polynomial and create a dmacs formula
 * from it **)

let generate_polynomials n_poly size nb_var max_degree debug= 
    let get_new_id = (get_new_var_func (nb_var+2)) in
    let aux_gen_poly () = 
        let p = generate_polynomial size nb_var max_degree in
        let fp = poly_to_formula p in
        (* negate formula because we want a valuation that is 0<->false*)
        let fpcnf, var_id = formula_to_cnf_formula (Not fp) (get_new_id) in
        (* X_nb_var+1 is Top thus it needs a clause to be forced true *)
        cnf_formula_to_cnf (And (And (fpcnf, var_id), Var (nb_var + 1)))
    and aux_gen_poly_debug () = 
        begin
            print_string "DEBUG MODE";
            print_string "\n**************************************\n";
            let p = generate_polynomial size nb_var max_degree in
            print_string (poly_to_string p);
            print_string "\n**************************************\n";
            let fp = poly_to_formula p in
            print_string (formula_to_string fp);
            print_string "\n**************************************\n";
            (* negate formula because we want a valuation that is 0<->false*)
            let fpcnf, var_id = formula_to_cnf_formula (Not fp) get_new_id in
            print_string (formula_to_string fpcnf);
            print_string "\n**************************************\n";
            (* X_nb_var+1 is Top thus it needs a clause to be forced true *)
            let cnfp = cnf_formula_to_cnf (And (And (fpcnf, var_id), Var (nb_var + 1))) in
            List.iter (fun c -> (print_string "[";
                        List.iter (fun e -> (print_string e; print_string " ")) c;
                        print_string "]\n")) cnfp;
            print_string "\n**************************************\n";
            cnfp
        end
    in
    let rec aux_to_dimacs n =
        match n, debug with 
        |0,_ -> []
        |k,true -> (aux_gen_poly_debug ())::(aux_to_dimacs (k-1))
        |k,false -> (aux_gen_poly ())::(aux_to_dimacs (k-1))
    in
    let lst_cnf = (aux_to_dimacs n_poly) in 
    cnf_to_dimacs ((get_new_id ()) - 1) lst_cnf;;



let () = 
    if Array.length Sys.argv < 5
    then
        print_string (String.concat " "
["Help:\n"; Sys.argv.(0) ; "<nb_polynomials> <size> <nb_var> <max_degree>\nAdd any flags after that to enable debug mode\n"])
    else
        let nb_poly = int_of_string Sys.argv.(1)
        and size = int_of_string Sys.argv.(2)
        and nb_var = int_of_string Sys.argv.(3)
        and max_degree = int_of_string Sys.argv.(4) in
        if Array.length Sys.argv = 5
        then
            print_string (generate_polynomials nb_poly size nb_var max_degree false)
        else
            print_string (generate_polynomials nb_poly size nb_var max_degree true)
